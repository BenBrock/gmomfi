import RPi.GPIO as GPIO
import time

RED_PIN = 22
GREEN_PIN = 17
BLUE_PIN = 27

CONNECTED_LIGHT = {'r': GPIO.HIGH, 'g': GPIO.LOW, 'b': GPIO.HIGH}
CONNECTING_LIGHT = {'r': GPIO.LOW, 'g': GPIO.LOW, 'b': GPIO.HIGH}
DISCONNECTED_LIGHT = {'r': GPIO.LOW, 'g': GPIO.HIGH, 'b': GPIO.HIGH}

class StatusLight:
    def __init__(self):
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(RED_PIN, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(GREEN_PIN, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(BLUE_PIN, GPIO.OUT, initial=GPIO.HIGH)

    def __del__(self):
        GPIO.cleanup()

    def output_color(self, pin_states):
        GPIO.output(RED_PIN, pin_states['r'])
        GPIO.output(GREEN_PIN, pin_states['g'])
        GPIO.output(BLUE_PIN, pin_states['b'])

    def set_connected(self):
        self.output_color(CONNECTED_LIGHT)

    def set_connecting(self):
        self.output_color(CONNECTING_LIGHT)

    def set_disconnected(self):
        self.output_color(DISCONNECTED_LIGHT)

    def cycle(self):
        for i in xrange(0, 4):
            for r in [GPIO.LOW, GPIO.HIGH]:
                for g in [GPIO.LOW, GPIO.HIGH]:
                    for b in [GPIO.LOW, GPIO.HIGH]:
                        if r == GPIO.LOW:
                            print "R",
                        if g == GPIO.LOW:
                            print "G",
                        if b == GPIO.LOW:
                            print "B",
                        print ""

                        self.output_color({'r': r, 'g': g, 'b': b})
                        time.sleep(0.5)

def test():
    status = StatusLight()

    status.set_disconnected()
    time.sleep(3.0)
    status.set_connecting()
    time.sleep(3.0)
    status.set_connected()
    time.sleep(3.0)
    time.sleep(10.0)
    status.cycle()
