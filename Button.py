import RPi.GPIO as GPIO
import thread
import time

BTN_PIN = 14

class Button:
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(BTN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.time_stamp = time.time()
        self.callback = None

    def __del__(self):
        GPIO.cleanup()

    def set_callback(self, callback):
        if self.callback is None:
            self.callback = callback
            GPIO.add_event_detect(BTN_PIN, GPIO.RISING, callback=self.callback_manager)
        else:
            print "Wut? Attempting to add two callbacks."

    def callback_manager(self, channel):
        time_now = time.time()
        if (time_now - self.time_stamp > 1.0):
            self.time_stamp = time_now
            thread.start_new_thread(self.callback, ())

def muh_print():
    print "Button pressed!"

def test():
    button = Button()
    button.set_callback(muh_print)
    time.sleep(100.0)
