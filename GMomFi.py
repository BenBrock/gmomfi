import Button as bh
import StatusLight as sl
import WvDial
import time
import os
import re

class GMomFi:
    def __init__(self):
        self.button = bh.Button()
        self.statuslight = sl.StatusLight()

        self.set_state_disconnected()
        self.ipv4_forward = False

        self.button.set_callback(self.switch_pressed)

    def switch_pressed(self):
        if self.state == "DISCONNECTED":
            self.connect()
        elif self.state == "CONNECTED":
            self.disconnect()

    def connect(self):
        self.set_state_connecting()

        os.system("sh /root/gmomfi/scripts/start_wifi.sh")

        wvdial_up = WvDial.connect()

        if not wvdial_up:
            self.set_state_disconnected()
            return

        if not self.ipv4_forward:
            os.system("sh /root/gmomfi/scripts/forward_ipv4.sh")
            self.ipv4_forward = True

        self.set_state_connected()

    def disconnect(self):
        self.set_state_connecting()

        WvDial.disconnect()

        os.system("sh /root/gmomfi/scripts/kill_wifi.sh")

        self.set_state_disconnected()

    def set_state_connected(self):
        self.state = "CONNECTED"
        self.statuslight.set_connected()

    def set_state_connecting(self):
        self.state = "CONNECTING"
        self.statuslight.set_connecting()

    def set_state_disconnected(self):
        self.state = "DISCONNECTED"
        self.statuslight.set_disconnected()

gmomfi = GMomFi()

while True:
    time.sleep(1000)
