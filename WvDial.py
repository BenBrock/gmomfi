import time
import os
import re

def connect():
    connected = False

    for i in xrange(0, 3):
        os.system("wvdial > /dev/null 2>&1 &")

        time.sleep(5)

        while not check_crashed():
            if check_interface("ppp"):
                connected = True
                break
            time.sleep(0.5)

        if connected:
            break

    if not connected and not check_crashed():
        disconnect()

    return connected

def disconnect():
    os.system("killall -15 wvdial")
    for i in xrange(0, 20):
        time.sleep(0.5)
        if check_crashed():
            break

    if not check_crashed():
        os.system("killall -9 wvdial")

def check_interface(interface):
    for line in os.popen('ifconfig'):
        if line.find(interface) != -1:
            return True
    return False

def check_crashed():
    for line in os.popen('ps aux | grep [w]vdial'):
        return False
    return True
